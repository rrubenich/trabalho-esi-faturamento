
package unioeste.faturamento.fabricante.col;

import java.sql.ResultSet;
import java.sql.SQLException;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.fabricante.Fabricante;
import unioeste.faturamento.exception.fabricante.FabricanteException;
import unioeste.faturamento.manager.UCFabricanteFaturamentoServicos;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.endereco.col.ColEndereco;
import unioeste.geral.exception.contato.ContatoException;
import unioeste.geral.exception.endereco.EnderecoException;
import unioeste.geral.manager.UCEnderecoGeralServicos;

public class ColFabricante {
    
	String query;
	
	public ResultSet consultaFabricantePorCod(Fabricante fab) throws SQLException {
		query = "SELECT * FROM fabricante WHERE "
				+ "idFabricante = " + fab.getId();
		
		ResultSet rs = Conexao.stm.executeQuery(query);
		
		return rs;
	}
	
	public ResultSet consultaFabricantePorNome(Fabricante fab) throws SQLException {
		query = "SELECT * FROM fabricante WHERE "
				+ "nomeFabricante = " + fab.getNome();
		
		ResultSet rs = Conexao.stm.executeQuery(query);
		
		return rs;
	}
	
	public ResultSet consultaFabricantePorCNPJ(Fabricante fab) throws SQLException {
		query = "SELECT * FROM fabricante WHERE "
				+ "cnpj = " + fab.getNumeroCNPJ().getNumeroDocFiscal();
		
		ResultSet rs = Conexao.stm.executeQuery(query);
		
		return rs;
	}
	
	public ResultSet consultaFabricantePorCidade(Fabricante fab) throws SQLException {
		
		query = "SELECT * FROM fabricante f, endereco e, enderecoCidade c WHERE "
				+ "f.idEndereco = e.idEndereco AND "
				+ "e.idCidade = c.idCidade AND "
				+ "f.idFabricante = " + fab.getId() + "AND "
				+ "e.nomeCidade = '" + fab.getEnderecoEspecifico().getEndereco().getCidade().getNome() + "'";
		
		ResultSet rs = Conexao.stm.executeQuery(query);
		
		return rs;
	}
	
	public Fabricante consultaCodFabricante(Fabricante fab) throws SQLException {
	            
        query = "SELECT * FROM fabricante WHERE "
                + "nomeFabricante     = '" + fab.getNome() + "' AND "
                + "cnpj     = '" + fab.getNumeroCNPJ().getNumeroDocFiscal() + "' AND "
                + "nomeFantasia = '" + fab.getSegundoNome() + "'";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
       
        fab.setId(rs.getInt("idFabricante"));
        
        return fab;
    }
	
	public Fabricante insereFabricante(Fabricante fab) throws SQLException, EnderecoException, ContatoException, FabricanteException{
		
		UCEnderecoGeralServicos ucEndereco = new UCEnderecoGeralServicos();
		
		Endereco end = ucEndereco.cadastrarEndereco(fab.getEnderecoEspecifico().getEndereco());
		end = ucEndereco.consultaCodEndereco(fab.getEnderecoEspecifico().getEndereco());
		fab.getEnderecoEspecifico().setEndereco(end);
		
		try{
			fab = consultaCodFabricante(fab);
		}
		catch(Exception e){
			query = "INSERT INTO fabricante "
		        	+ "(nomeFabricante, enderecoComplemento, enderecoNumero, nomeFantasia, idEndereco, cnpj) "
		            + "VALUES "
		            + "('" 
		            + fab.getNome() + "','" 
		            + fab.getEnderecoEspecifico().getComplemento() + "',"
		            + fab.getEnderecoEspecifico().getNumero() + ",'" 
		            + fab.getSegundoNome() + "'," 
		            + fab.getEnderecoEspecifico().getEndereco().getIdEndereco() + ",'" 
		            + fab.getNumeroCNPJ().getNumeroDocFiscal() + "')";  
		        
	        Conexao.stm.executeUpdate(query);
	        
	        fab = consultaCodFabricante(fab);
	        
	        ColEmailFabricante cEF = new ColEmailFabricante();
	        fab = cEF.insereEmailFabricante(fab);
	        
	        ColTelefoneFabricante cTF = new ColTelefoneFabricante();
	        fab = cTF.insereTelefoneFabricante(fab);
	        
	        fab = consultaCodFabricante(fab);
		}

		return fab;
    }
	
	public Fabricante alterarFabricante(Fabricante fab) throws SQLException, ContatoException{
		
		ColEndereco cE = new ColEndereco();
		UCFabricanteFaturamentoServicos ucFab = new UCFabricanteFaturamentoServicos();
		
		try{
			Endereco end = cE.consultaCodEndereco(fab.getEnderecoEspecifico().getEndereco());
			fab.getEnderecoEspecifico().setEndereco(end);
		}
		catch(SQLException dr){
			Endereco end = cE.insereEndereco(fab.getEnderecoEspecifico().getEndereco());
			end = cE.consultaCodEndereco(fab.getEnderecoEspecifico().getEndereco());
			fab.getEnderecoEspecifico().setEndereco(end);
		}
		
		try{
			fab = ucFab.obterFabricantePorPK(fab);
		}
		catch(Exception e){
		    query = "UPDATE fabricante SET "
		    	+ "nomeFabricante = '" + fab.getNome() 
		    	+ "', enderecoComplemento = '" + fab.getEnderecoEspecifico().getComplemento()
		    	+ "', enderecoNumero = " + fab.getEnderecoEspecifico().getNumero()
		    	+ ", nomeFantasia = '" + fab.getSegundoNome()
		    	+ "', idEndereco = " + fab.getEnderecoEspecifico().getEndereco().getIdEndereco()
		    	+ ", cnpj = '" + fab.getNumeroCNPJ().getNumeroDocFiscal()
		    	+ "' WHERE "
		    	+ " idFabricante = " + fab.getId();
		         
		    ColEmailFabricante cEF = new ColEmailFabricante();
		    fab = cEF.excluiTodosEmailFabricante(fab);
		    fab = cEF.insereEmailFabricante(fab);
		    
		    ColTelefoneFabricante cTF = new ColTelefoneFabricante();
		    cTF.excluiTodosTelefoneFabricante(fab);
		    fab = cTF.insereTelefoneFabricante(fab);
		
		    Conexao.stm.executeUpdate(query);
		}
		
		return fab;
    }
	
    public Fabricante excluirFabricante(Fabricante fab) throws SQLException {

    	ColTelefoneFabricante cTF = new ColTelefoneFabricante();
    	cTF.excluiTodosTelefoneFabricante(fab);
    	
    	ColEmailFabricante cEF = new ColEmailFabricante();
    	cEF.excluiTodosEmailFabricante(fab);
    	
        query = "DELETE FROM fabricante WHERE idFabricante = " + fab.getId();
        Conexao.stm.executeUpdate(query);

        return fab;
    }
    
    public ResultSet resultSetFabricante() throws SQLException {
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM fabricante");
        return rs;
    }
    
    public Fabricante dePara(ResultSet rsFabricante) throws SQLException {
    	ColTelefoneFabricante cTF = new ColTelefoneFabricante();
    	ColEmailFabricante cEF = new ColEmailFabricante();
    	UCEnderecoGeralServicos ucEGS = new UCEnderecoGeralServicos();

    	Fabricante fab = new Fabricante();
    	
    	fab.setId(rsFabricante.getInt("idFabricante"));
    	fab.setNome(rsFabricante.getString("nomeFabricante"));
    	fab.getEnderecoEspecifico().setComplemento(rsFabricante.getString("enderecoComplemento"));
    	fab.getEnderecoEspecifico().setNumero(rsFabricante.getInt("enderecoNumero"));
    	fab.setSegundoNome(rsFabricante.getString("nomeFantasia"));
    	fab.getNumeroCNPJ().setNumeroDocFiscal(rsFabricante.getString("cnpj"));

    	Endereco end = new Endereco();
    	end.setIdEndereco(rsFabricante.getInt("idEndereco"));
    	ucEGS.obterEnderecoPorID(end);
    	
    	fab = cTF.obterListaTelefonesFabricante(fab);
    	fab = cEF.obterListaEmailFabricante(fab);
    	
    	return fab;
    }
	
}
