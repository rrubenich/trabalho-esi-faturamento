/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.faturamento.fabricante.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.fabricante.Fabricante;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.contato.TipoTelefone;
import unioeste.geral.contato.col.ColTipoTelefone;
import unioeste.geral.exception.contato.ContatoException;

public class ColTelefoneFabricante {
	String query;
	
	public Fabricante consultaTelefoneFabricante(Fabricante fab) throws SQLException {    
        query = "SELECT * FROM telefoneFabricante WHERE "
                + "idFabricante     = " + fab.getId();

        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        ArrayList<Telefone> telefones = new ArrayList<Telefone>();
        while(rs.next()){
        	Telefone t = new Telefone();
        	t.setDDD(rs.getInt("ddd"));
        	t.setNumero(rs.getString("telefone"));
        	
        	TipoTelefone tt = new TipoTelefone();
        	tt.setIdTipoTelefone(rs.getInt("idTipoFone"));
        	ColTipoTelefone cTF = new ColTipoTelefone();
        	
        	try{
	        	tt = cTF.consultaTipoTelefone(tt);
        	}
        	catch(SQLException sql){
        		tt = cTF.insereTipoTelefone(tt);
        		tt = cTF.consultaCodTipoTelefone(tt);
        	}
        	
        	t.setTipoTelefone(tt);
        	
        	telefones.add(t);
        }
        
        fab.setTelefones(telefones);
        
        return fab;
	}
	
	public Fabricante insereTelefoneFabricante(Fabricante fab) throws SQLException, ContatoException {
		
		ColTipoTelefone cTT = new ColTipoTelefone();
		
		for(Telefone t :fab.getTelefones()){
			try{
				t.setTipoTelefone(cTT.consultaCodTipoTelefone(t.getTipoTelefone()));
			}
			catch(Exception e){
				cTT.insereTipoTelefone(t.getTipoTelefone());
				t.setTipoTelefone(cTT.consultaCodTipoTelefone(t.getTipoTelefone()));
			}
			
			query = "INSERT INTO telefoneFabricante "
					+ "(telefone, idFabricante, ddd, idTipoFone) "
					+ "VALUES ('"
					+ t.getNumero() + "',"
					+ fab.getId() + ","
					+ t.getDDD() + ","
					+ t.getTipoTelefone().getIdTipoTelefone() + ")";
			
			Conexao.stm.executeUpdate(query);
		}
		
		return fab;
	}
	
	public Fabricante excluiTodosTelefoneFabricante(Fabricante fab) throws SQLException {
		query = "DELETE FROM telefoneFabricante WHERE "
				+ "idFabricante = " + fab.getId();
		
		Conexao.stm.executeUpdate(query);
		
		return fab;
	}
	
	public ResultSet resultSetTelefoneFabricante(Fabricante fab) throws SQLException {
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM telefoneFabricante WHERE idFabricante = " + fab.getId());
        return rs;
    }
	
	public Fabricante obterListaTelefonesFabricante(Fabricante fab) throws SQLException {
		ColTipoTelefone cTT = new ColTipoTelefone();
		
		ResultSet rsTelefoneFabricante = resultSetTelefoneFabricante(fab);
        while(rsTelefoneFabricante.next()){
        	Telefone t = new Telefone();
        	t.setDDD(rsTelefoneFabricante.getInt("ddd"));
        	t.setNumero(rsTelefoneFabricante.getString("telefone"));
        	
        	TipoTelefone tt = new TipoTelefone();
        	tt.setIdTipoTelefone(rsTelefoneFabricante.getInt("idTipoFone"));
        	t.setTipoTelefone(cTT.consultaTipoTelefone(tt));	
        	
        	fab.getTelefones().add(t);
        }
        
		return fab;
	}

}
