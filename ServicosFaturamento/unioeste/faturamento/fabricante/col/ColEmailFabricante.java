
package unioeste.faturamento.fabricante.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.fabricante.Fabricante;
import unioeste.geral.bo.contato.Email;


public class ColEmailFabricante {
	String query;
	
	public Fabricante consultaEmailFabricante(Fabricante fab) throws SQLException {
		query = "SELECT * FROM emailFabricante WHERE "
                + "idFabricante     = " + fab.getId();

        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        ArrayList<Email> emails = new ArrayList<Email>();
        while(rs.next()){
        	Email e = new Email();
        	e.setEmail(rs.getString("email"));
        	emails.add(e);
        }
        
        fab.setEmails(emails);
        
        return fab;
	}
	
	public Fabricante insereEmailFabricante(Fabricante fab) throws SQLException {
		
		for(Email e :fab.getEmails()){
			query = "INSERT INTO emailFabricante "
					+ "(email, idFabricante) "
					+ "VALUES ('"
					+ e.getEmail() + "',"
					+ fab.getId() + ")";
			
			Conexao.stm.executeUpdate(query);
		}
		
		return fab;
	}
	
	public Fabricante excluiTodosEmailFabricante(Fabricante fab) throws SQLException {
		query = "DELETE FROM emailFabricante WHERE "
				+ "idFabricante = " + fab.getId();
		
		Conexao.stm.executeUpdate(query);
		
		return fab;
	}
	
	public ResultSet resultSetEmailFabricante(Fabricante fab) throws SQLException {
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM emailFabricante WHERE idFabricante = " + fab.getId());
        return rs;
    }
	
	public Fabricante obterListaEmailFabricante(Fabricante fab) throws SQLException {
		ResultSet rsEmailFabricante = resultSetEmailFabricante(fab);
        while(rsEmailFabricante.next()){
        	Email e = new Email();
        	e.setEmail(rsEmailFabricante.getString("email"));
        	
        	fab.getEmails().add(e);
        }
        
		return fab;
	}
}
