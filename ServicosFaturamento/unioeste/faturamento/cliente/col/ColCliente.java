package unioeste.faturamento.cliente.col;

import java.sql.ResultSet;
import java.sql.SQLException;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.cliente.*;
import unioeste.faturamento.exception.cliente.ClienteException;
import unioeste.geral.exception.endereco.EnderecoException;
import unioeste.geral.exception.pessoa.PessoaException;
import unioeste.geral.bo.documento.*;
import unioeste.geral.bo.endereco.*;
import unioeste.geral.bo.pessoa.*;
import unioeste.geral.manager.*;


public class ColCliente {
	
	String query;
	
    public Cliente consultaClienteDocFiscal(Cliente cli) throws SQLException, PessoaException {
        
    	if(cli.getDadosClienteFisico() != null){

			String cpf = cli.getDadosClienteFisico().getNumeroCPF().getNumeroDocFiscal();
			
			query = "SELECT * FROM cliente WHERE CPF = " + cpf;
	
    	}else{

			String cnpj = cli.getDadosClienteJuridico().getNumeroCNPJ().getNumeroDocFiscal();
			
			query = "SELECT * FROM cliente WHERE CNPJ = " + cnpj;

    	}

        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();

        cli = dePara(rs);
        
        return cli;
    }
    
    public Cliente obterClientePorId(Cliente cli) throws SQLException, PessoaException {
        
    	query = "SELECT * FROM cliente WHERE idCliente = '" + cli.getIdCliente() + "'";

        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        cli = dePara(rs);
        
        return cli;
    }
    
    public Cliente insereCliente(Cliente cli) throws SQLException, ClienteException, EnderecoException, PessoaException{
    	
    	ColEmailCliente cEC			  = new ColEmailCliente();
    	ColTelefoneCliente cTC		  = new ColTelefoneCliente();
    	UCEnderecoGeralServicos ucEnd = new UCEnderecoGeralServicos();
    	UCOrgaoExpGeralServicos	ucOrg = new UCOrgaoExpGeralServicos();

    	cli = cEC.insereEmailCliente(cli);
		cli = cTC.insereTelefoneCliente(cli);
    	
		
    	try{
    		
    		cli = consultaClienteDocFiscal(cli);
    		
    		throw new ClienteException("Cliente já cadastrado");
    		
    	}catch(SQLException e){
    		
    		if(cli.getDadosClienteFisico() != null){
    			
    			ClienteFisico cliF = cli.getDadosClienteFisico();
    			
    			Endereco end = ucEnd.cadastrarEndereco(cliF.getEnderecoEspecifico().getEndereco());
    			cliF.getEnderecoEspecifico().setEndereco(end);
    			
    			try{
    				OrgaoExp org = ucOrg.obterOrgaoExpPorID(cliF.getNumeroDocNacional().getDocIDBrasil().getOrgaoExp());
        			cliF.getNumeroDocNacional().getDocIDBrasil().setOrgaoExp(org);
    			}
    			catch(SQLException ex){
    				OrgaoExp org = ucOrg.cadastrarOrgaoExp(cliF.getNumeroDocNacional().getDocIDBrasil().getOrgaoExp());
        			cliF.getNumeroDocNacional().getDocIDBrasil().setOrgaoExp(org);
    			}
    			
    			query = "INSERT INTO cliente "
    					+ "(nomeCliente, segundoNome, CPF, CNPJ, sexo, RG, "
    					+ "enderecoComplemento, enderecoNumero, idEndereco, idOrgao) "
    					+ "VALUES (" 
    					+ cliF.getNome() + "','" 
    					+ cliF.getSegundoNome() + "','" 
    					+ cliF.getNumeroCPF() 
    					+ "',null,'" 
    					+ cliF.getSexo().getSigla() + "','" 
    					+ cliF.getNumeroDocNacional().getDocIDBrasil() + "','" 
    					+ cliF.getEnderecoEspecifico().getComplemento() + "','" 
    					+ cliF.getEnderecoEspecifico().getNumero() + "','" 
    					+ cliF.getEnderecoEspecifico().getEndereco().getIdEndereco() + "','" 
    					+ cliF.getNumeroDocNacional().getDocIDBrasil().getOrgaoExp().getIdOrgaoExp() + "')";
				
        	}else{

    			ClienteJuridico cliJ = cli.getDadosClienteJuridico();
    			
    			Endereco end = ucEnd.cadastrarEndereco(cliJ.getEnderecoEspecifico().getEndereco());
    			cliJ.getEnderecoEspecifico().setEndereco(end);
    			
    			query = "INSERT INTO cliente "
    					+ "(nomeCliente, segundoNome, CPF, CNPJ, sexo, "
    					+ "RG, enderecoComplemento, enderecoNumero, idEndereco, idOrgao) "
    					+ "VALUES (" 
    					+ cliJ.getNome() + "','" 
    					+ cliJ.getSegundoNome() + "','" 
    					+ "',null,'" 
    					+ cliJ.getNumeroCNPJ() 
    					+ "',null,'" 
    					+ "',null,'" 
    					+ cliJ.getEnderecoEspecifico().getComplemento() + "','" 
    					+ cliJ.getEnderecoEspecifico().getNumero() + "','" 
    					+ cliJ.getEnderecoEspecifico().getEndereco().getIdEndereco() 
    					+ "',null)";
        	}
    		
			Conexao.stm.executeUpdate(query);
    	}
    	
    	return cli;
    }
    
    public Cliente dePara(ResultSet rs) throws SQLException, PessoaException{

    	String nome						= rs.getString("nomeCliente");
    	String segundoNome  			= rs.getString("segundoNome");
    	String enderecoComplemento  	= rs.getString("enderecoComplemento");
    	int enderecoNumero 		  		= rs.getInt("endeceoNumero");
    	int idEndereco  		  		= rs.getInt("idEndereco");
    	
    	Endereco end 			  		= new Endereco();
    	EnderecoEspecifico endEsp 		= new EnderecoEspecifico();
    	UCEnderecoGeralServicos ucEnd 	= new UCEnderecoGeralServicos();

    	
    	end.setIdEndereco(idEndereco);
    	endEsp.setEndereco(end);
    	endEsp.setComplemento(enderecoComplemento);
    	endEsp.setNumero(enderecoNumero);
    	
    	end = ucEnd.obterEnderecoPorID(end);
    	

		Cliente cli = new Cliente();
    	
    	
    	if(rs.getString("CNPJ") == null){
    		
    		ClienteFisico cf;
    		
    		String numeroCpf  = rs.getString("CPF");
    		String siglaSexo  = rs.getString("sexo");
    		String numeroRG   = rs.getString("RG");
    		int idOrgao 	  = rs.getInt("idOrgao");
    		
        	Sexo sexo						= new Sexo();
        	OrgaoExp orgaoExp		 		= new OrgaoExp();
        	UCSexoGeralServicos ucSexo		= new UCSexoGeralServicos();
        	UCOrgaoExpGeralServicos ucOrgao = new UCOrgaoExpGeralServicos();
        	CPF cpf							= new CPF(numeroCpf);

        	sexo.setSigla(siglaSexo);
    		orgaoExp.setIdOrgaoExp(idOrgao);
    		
    		sexo = ucSexo.obterSexoCliente(sexo);
    		orgaoExp = ucOrgao.obterOrgaoExpPorID(orgaoExp);
    		
    		DocIDBrasil docIDBrasil = new DocIDBrasil(numeroRG, orgaoExp);
    		DocID docID = new DocID(docIDBrasil);
    		
    		ClienteFisico cliF = new ClienteFisico(nome, segundoNome, endEsp, null, null, cpf, sexo, docID);
    		
    		cli.setDadosClienteFisico(cliF);
    		
    	}else{
    		String numeroCnpj = rs.getString("CNPJ");
 
    		CNPJ cnpj = new CNPJ(numeroCnpj);
    		
    		ClienteJuridico cliJ = new ClienteJuridico(nome, segundoNome, endEsp, null, null, cnpj);
    		
    		cli.setDadosClienteJuridico(cliJ);
    	}

        
        return cli;
    }

    public ResultSet resultSetCliente() throws SQLException {
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM cliente");
        return rs;
    }
	
	public ResultSet consultaClientePorNome(Cliente cli) throws SQLException {
		
		String nome;
		
		if(cli.getDadosClienteFisico() != null){
			nome = cli.getDadosClienteFisico().getNome();
		}
		else{
			nome = cli.getDadosClienteJuridico().getNome();
		}
		query = "SELECT * FROM cliente WHERE nomeCliente = " + nome;
		
		ResultSet rs = Conexao.stm.executeQuery(query);
		
		return rs;
	}

	public ResultSet consultaClientePorCidade(Cliente cli) throws SQLException {

		String nomeCidade;
		
		if(cli.getDadosClienteFisico() != null){
			nomeCidade = cli.getDadosClienteFisico().getEnderecoEspecifico().getEndereco().getCidade().getNome();
		}
		else{
			nomeCidade = cli.getDadosClienteJuridico().getEnderecoEspecifico().getEndereco().getCidade().getNome();
		}
		query = "SELECT * FROM cliente f, endereco e, enderecoCidade c WHERE "
				+ "f.idEndereco = e.idEndereco AND "
				+ "e.idCidade = c.idCidade AND "
				+ "f.idCliente = " + cli.getIdCliente() + "AND "
				+ "e.nomeCidade = '" + nomeCidade + "'";
		
		ResultSet rs = Conexao.stm.executeQuery(query);
		
		return rs;
	}
	
    public Cliente excluirCliente(Cliente cli) throws SQLException {

    	ColTelefoneCliente cTC = new ColTelefoneCliente();
    	cTC.excluiTelefonesCliente(cli);
    	
    	ColEmailCliente cEC = new ColEmailCliente();
    	cEC.excluiEmailsCliente(cli);
    	
        query = "DELETE FROM Cliente WHERE idCliente = " + cli.getIdCliente();
        Conexao.stm.executeUpdate(query);

        return cli;	
    }
}
