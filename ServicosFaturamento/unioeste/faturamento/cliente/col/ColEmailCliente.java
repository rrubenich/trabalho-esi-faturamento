package unioeste.faturamento.cliente.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.cliente.Cliente;
import unioeste.geral.bo.contato.Email;

public class ColEmailCliente {
	String query;
	
	public Cliente consultaEmailCliente(Cliente cli) throws SQLException {    

        ResultSet rs = resultSetEmailCliente(cli);
        rs.last();
        
        ArrayList<Email> emails = new ArrayList<Email>();
        while(rs.next()){
        	Email em = new Email();
        	em.setEmail(rs.getString("email"));
        	emails.add(em);
        }
        

    	if(cli.getDadosClienteFisico() != null){
    		cli.getDadosClienteFisico().setEmails(emails);
    	}else{
    		cli.getDadosClienteJuridico().setEmails(emails);
    	}
        
        return cli;
	}
	
	public Cliente insereEmailCliente(Cliente cli) throws SQLException {
		
		if(cli.getDadosClienteFisico() != null){
			for(Email em :cli.getDadosClienteFisico().getEmails()){
				
				query = "INSERT INTO emailCliente (emailCliente, idCliente) VALUES ('" + em.getEmail() + "'," + cli.getIdCliente() + ")";
				
				Conexao.stm.executeUpdate(query);
			}
    	}else{
    		for(Email em :cli.getDadosClienteJuridico().getEmails()){
    			
    			query = "INSERT INTO emailCliente (emailCliente, idCliente) VALUES ('" + em.getEmail() + "'," + cli.getIdCliente() + ")";
    			
    			Conexao.stm.executeUpdate(query);
    		}
    	}
		
		return cli;
	}
	
	public Cliente excluiEmailsCliente(Cliente cli) throws SQLException {
		query = "DELETE FROM emailCliente WHERE"
				+ "idCliente = " + cli.getIdCliente();
		
		Conexao.stm.executeUpdate(query);
		
		return cli;
	}
	
	public ResultSet resultSetEmailCliente(Cliente cli) throws SQLException {
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM emailCliente WHERE idCliente = " + cli.getIdCliente());
        return rs;
    }

}
