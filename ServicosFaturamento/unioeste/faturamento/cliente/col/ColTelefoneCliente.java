package unioeste.faturamento.cliente.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.cliente.Cliente;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.contato.TipoTelefone;
import unioeste.geral.contato.col.ColTipoTelefone;

public class ColTelefoneCliente {
	
	String query; 
	
	public Cliente consultaTelefoneCliente(Cliente cli) throws SQLException {
		
        query = "SELECT * FROM telefoneCliente WHERE idCliente = " + cli.getIdCliente();

        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        ArrayList<Telefone> telefones = new ArrayList<Telefone>();
        while(rs.next()){
        	Telefone t = new Telefone();
        	t.setDDD(rs.getInt("ddd"));
        	t.setNumero(rs.getString("telefone"));
        	
        	TipoTelefone tt = new TipoTelefone();
        	tt.setIdTipoTelefone(rs.getInt("idTipoFone"));
        	ColTipoTelefone cTF = new ColTipoTelefone();
        	
        	try{
	        	tt = cTF.consultaTipoTelefone(tt);
        	}
        	catch(SQLException sql){
        		tt = cTF.insereTipoTelefone(tt);
        		tt = cTF.consultaCodTipoTelefone(tt);
        	}
        	
        	t.setTipoTelefone(tt);
        	
        	telefones.add(t);
        }
        
        if(cli.getDadosClienteFisico() != null){
    		cli.getDadosClienteFisico().setTelefones(telefones);
    	}else{
    		cli.getDadosClienteJuridico().setTelefones(telefones);
    	}
        
        return cli;
	}
	
	public Cliente insereTelefoneCliente(Cliente cli) throws SQLException {

		ArrayList<Telefone> telefones = new ArrayList<Telefone>();
		
		if(cli.getDadosClienteFisico() != null){
			telefones = cli.getDadosClienteFisico().getTelefones();
    	}else{
    		telefones = cli.getDadosClienteJuridico().getTelefones();
    	}
		
		for(Telefone t: telefones){
			query = "INSERT INTO telefoneCliente "
					+ "(telefone, idCliente, ddd, idTipoFone) "
					+ "VALUES ('"
					+ t.getNumero() + "',"
					+ cli.getIdCliente() + ","
					+ t.getDDD() + ","
					+ t.getTipoTelefone().getIdTipoTelefone() + ")";
			
			Conexao.stm.executeUpdate(query);
		}
		
		return cli;
	}
	
	public Cliente excluiTelefonesCliente(Cliente cli) throws SQLException {
		query = "DELETE FROM telefoneCliente WHERE idCliente = " + cli.getIdCliente();
		
		Conexao.stm.executeUpdate(query);
		
		return cli;
	}
	
	public ResultSet resultSetTelefoneCliente(Cliente cli) throws SQLException {
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM telefoneCliente WHERE idCliente = " + cli.getIdCliente());
        return rs;
    }
	
	public Cliente obterTelefonesCliente(Cliente cli) throws SQLException {
		ColTipoTelefone cTT = new ColTipoTelefone();
		
		ArrayList<Telefone> telefones = new ArrayList<Telefone>();
		
		ResultSet rsTelefoneCliente = resultSetTelefoneCliente(cli);
        while(rsTelefoneCliente.next()){
        	Telefone t = new Telefone();
        	t.setDDD(rsTelefoneCliente.getInt("ddd"));
        	t.setNumero(rsTelefoneCliente.getString("telefone"));
        	
        	TipoTelefone tt = new TipoTelefone();
        	tt.setIdTipoTelefone(rsTelefoneCliente.getInt("idTipoFone"));
        	t.setTipoTelefone(cTT.consultaTipoTelefone(tt));	
        	
        	telefones.add(t);
        	
    		if(cli.getDadosClienteFisico() != null){
    			cli.getDadosClienteFisico().setTelefones(telefones);
        	}else{
        		cli.getDadosClienteJuridico().setTelefones(telefones);
        	}
        }
        
		return cli;
	}
}
