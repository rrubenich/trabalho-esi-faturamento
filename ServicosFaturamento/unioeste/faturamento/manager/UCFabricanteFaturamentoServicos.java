/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.faturamento.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import unioeste.faturamento.bo.fabricante.Fabricante;
import unioeste.faturamento.exception.fabricante.FabricanteException;
import unioeste.faturamento.fabricante.col.ColFabricante;
import unioeste.geral.exception.contato.ContatoException;
import unioeste.geral.exception.endereco.EnderecoException;

public class UCFabricanteFaturamentoServicos {
    
	String query;
	
    public Fabricante cadastrarFabricante(Fabricante fab) throws SQLException, EnderecoException, ContatoException, FabricanteException{

    	ColFabricante colFabricante = new ColFabricante();
    	colFabricante.insereFabricante(fab);

        return fab;
    }
    
    public Fabricante alterarFabricante(Fabricante fab) throws SQLException, ContatoException{
        
    	ColFabricante colFabricante = new ColFabricante();
    	colFabricante.alterarFabricante(fab);
    	
        return fab;
    }
    
    public Fabricante excluirFabricante(Fabricante fab) throws SQLException{
        
    	ColFabricante colFabricante = new ColFabricante();
    	colFabricante.excluirFabricante(fab);
    	
        return fab;
    }
    
    public Fabricante obterFabricantePorPK(Fabricante fab) throws SQLException{
    	ColFabricante cF = new ColFabricante();
    	
    	ResultSet rsFabricante = cF.consultaFabricantePorCod(fab);
    	rsFabricante.last();

    	fab = cF.dePara(rsFabricante);
    	
        return fab;
    }
    
    public ArrayList<Fabricante> obterListaFabricante() throws SQLException{
    	
    	ColFabricante cF = new ColFabricante();
    	
        ArrayList<Fabricante> fabricantes = new ArrayList<Fabricante>();
    	
        ResultSet rsFabricante = cF.resultSetFabricante();
        while(rsFabricante.next()){
        	Fabricante fab = new Fabricante();
        	fab = cF.dePara(rsFabricante);
        	fabricantes.add(fab);
        }
 
    	return fabricantes;
    }
    
    public Fabricante obterFabricanteCNPJ(Fabricante fab) throws SQLException{
    	ColFabricante cF = new ColFabricante();
    	
    	ResultSet rsFabrincante = cF.consultaFabricantePorCNPJ(fab);
    	rsFabrincante.last();
    	
    	fab = cF.dePara(rsFabrincante);
    	
    	return fab;
    }
    
    public ArrayList<Fabricante> obterListaFabricanteNome(Fabricante fab) throws SQLException{
    	
    	//manda fabricante com nomeFabricante
    	
    	ColFabricante cF = new ColFabricante();
    	
        ArrayList<Fabricante> fabricantes = new ArrayList<Fabricante>();
    	
        ResultSet rsFabricante = cF.consultaFabricantePorNome(fab);
        while(rsFabricante.next()){
        	Fabricante f = new Fabricante();
        	f = cF.dePara(rsFabricante);
        	
        	fabricantes.add(f);
        }
 
    	return fabricantes;
    }
    
    public ArrayList<Fabricante> obterListaFabricanteCidade(Fabricante fab) throws SQLException{
    	
    	//manda fabricante com nomeCidade, idFabricante, idEndereco
    	
    	ColFabricante cF = new ColFabricante();
    	
        ArrayList<Fabricante> fabricantes = new ArrayList<Fabricante>();
    	
        ResultSet rsFabricante = cF.consultaFabricantePorCidade(fab);
        while(rsFabricante.next()){
        	Fabricante f = new Fabricante();
        	f = cF.dePara(rsFabricante);
        	
        	fabricantes.add(f);
        }
 
    	return fabricantes;
    }
}
