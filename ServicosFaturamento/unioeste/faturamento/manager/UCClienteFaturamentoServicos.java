package unioeste.faturamento.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import unioeste.faturamento.bo.cliente.Cliente;
import unioeste.faturamento.cliente.col.ColCliente;
import unioeste.faturamento.exception.cliente.ClienteException;
import unioeste.geral.exception.endereco.EnderecoException;
import unioeste.geral.exception.pessoa.PessoaException;

public class UCClienteFaturamentoServicos {
    
    
    public Cliente cadastrarCliente(Cliente cli) throws ClienteException, SQLException, EnderecoException, PessoaException{
        
    	cli = cli.validaCliente(cli);
    	
    	ColCliente cc = new ColCliente();
    	
    	cli = cc.insereCliente(cli);
    	
        return cli;
    }

    public Cliente excluirCliente(Cliente cli) throws SQLException{
        
    	ColCliente cC = new ColCliente();
    	cC.excluirCliente(cli);
    	
        return cli;
    }
    
    public Cliente obterClientePorPK(Cliente cli) throws SQLException, PessoaException{
    	
    	ColCliente cc = new ColCliente();

    	cli = cc.obterClientePorId(cli);
    	
        return cli;
    }
    
    public Cliente obterClientePorDocFiscal(Cliente cli) throws SQLException, PessoaException{
    	
    	ColCliente cc = new ColCliente();

    	cli = cc.consultaClienteDocFiscal(cli);
    	
        return cli;
    }
    
    public ArrayList<Cliente> obterListaCliente() throws SQLException, PessoaException{
    	
    	ColCliente cF = new ColCliente();
    	
        ArrayList<Cliente> Clientes = new ArrayList<Cliente>();
    	
        ResultSet rsCliente = cF.resultSetCliente();
        
        while(rsCliente.next()){
        	Cliente cli = new Cliente();
        	cli = cF.dePara(rsCliente);
        	Clientes.add(cli);
        }
 
    	return Clientes;
    }

    public ArrayList<Cliente> obterListaClienteNome(Cliente cli) throws SQLException, PessoaException{
    	
    	ColCliente cF = new ColCliente();
    	
        ArrayList<Cliente> Clientes = new ArrayList<Cliente>();
    	
        ResultSet rsCliente = cF.consultaClientePorNome(cli);
        while(rsCliente.next()){
        	Cliente f = new Cliente();
        	f = cF.dePara(rsCliente);
        	Clientes.add(f);
        }
 
    	return Clientes;
    }

    public ArrayList<Cliente> obterListaClienteCidade(Cliente cli) throws SQLException, PessoaException{
    	
    	ColCliente cC = new ColCliente();
    	
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    	
        ResultSet rsCliente = cC.consultaClientePorCidade(cli);
        
        while(rsCliente.next()){
        	Cliente c = new Cliente();
        	c = cC.dePara(rsCliente);
        	clientes.add(c);
        }
 
    	return clientes;
    }
}
