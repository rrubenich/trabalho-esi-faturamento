package unioeste.geral.exception.pessoa;

public class PessoaException extends Exception {
    private String mensagem;
    
    public PessoaException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
}
