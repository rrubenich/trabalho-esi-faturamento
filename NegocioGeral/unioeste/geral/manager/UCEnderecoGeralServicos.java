package unioeste.geral.manager;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.endereco.col.ColEndereco;
import unioeste.geral.exception.endereco.EnderecoException;

public class UCEnderecoGeralServicos {

    String query;
    int insere;
    
    public Endereco cadastrarEndereco(Endereco end) throws EnderecoException, SQLException{
        
        end.validaCep(end.getCep());
        end.validaEndereco(end);
        
        ColEndereco colEndereco = new ColEndereco();
        
        colEndereco.insereEndereco(end);

        return end;
        
    }
    
    public Endereco obterEnderecoPorCep(Endereco end) throws EnderecoException, SQLException {

        end.validaCep(end.getCep());
        
        ColEndereco cE = new ColEndereco();
        
        ResultSet rs = cE.resultSetEndereco(end.getCep());
        rs.last();
        
        end = cE.dePara(rs);

        return end;
    }

    public Endereco obterEnderecoPorID(Endereco end) throws SQLException {
    	ColEndereco cE = new ColEndereco();
        
        ResultSet rs = cE.resultSetEndereco(end.getIdEndereco());
        rs.last();
        
        end = cE.dePara(rs);

        return end;
    }
    
    public Endereco excluirEndereco(Endereco end) throws SQLException {
        
        ColEndereco colEndereco = new ColEndereco();
        
        colEndereco.excluirEndereco(end);
        
        return end;
    }
    
    public Endereco alterarEndereco(Endereco end) throws SQLException {
    	ColEndereco colEndereco = new ColEndereco();
        
        colEndereco.alterarEndereco(end);
        
        return end;
    }

    public Endereco obterEnderecoPorSite(String href) throws EnderecoException, IOException {
        
        ColEndereco cE = new ColEndereco();
        
        return cE.consultaEnderecoPorSite(href);
    }

    public Endereco consultaCodEndereco(Endereco end) throws SQLException{

        ColEndereco colEndereco = new ColEndereco();
        
        colEndereco.consultaCodEndereco(end);

        return end;
    }
}
