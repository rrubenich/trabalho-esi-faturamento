/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.manager;

import java.util.ArrayList;

import unioeste.geral.bo.pessoa.Sexo;
import unioeste.geral.exception.pessoa.PessoaException;
import unioeste.geral.sexo.col.ColSexo;

public class UCSexoGeralServicos {
    public Sexo obterSexoCliente(Sexo sexo) throws PessoaException{
    	
    	ColSexo cS = new ColSexo();
    	
    	sexo.validaSexo(sexo);
    	
    	sexo = cS.consultaSexo(sexo);
    	
        return sexo;
    }
    
   public ArrayList<Sexo> obterListaSexo(){

   		ColSexo cS = new ColSexo();
 
    	return cS.obterListaSexo();
    }
}
