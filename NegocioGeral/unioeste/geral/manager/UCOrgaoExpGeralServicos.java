package unioeste.geral.manager;

import java.sql.ResultSet;
import java.sql.SQLException;

import unioeste.geral.bo.documento.OrgaoExp;
import unioeste.geral.documento.col.ColOrgaoExp;


public class UCOrgaoExpGeralServicos {
    public OrgaoExp cadastrarOrgaoExp(OrgaoExp orgaoExp) throws SQLException{
        
        ColOrgaoExp colOrgaoExp = new ColOrgaoExp();
        
        colOrgaoExp.insereOrgaoExp(orgaoExp);

        return orgaoExp;
    }
    
    public OrgaoExp obterOrgaoExpPorID(OrgaoExp orgaoExp) throws SQLException {
    	ColOrgaoExp cE = new ColOrgaoExp();
        
        ResultSet rs = cE.resultSetOrgaoExp(orgaoExp.getIdOrgaoExp());
        rs.last();
        
        orgaoExp = cE.dePara(rs);

        return orgaoExp;
    }
}
