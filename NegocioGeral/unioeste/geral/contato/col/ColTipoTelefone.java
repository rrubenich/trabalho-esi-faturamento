
package unioeste.geral.contato.col;

import java.sql.ResultSet;
import java.sql.SQLException;

import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.contato.TipoTelefone;

public class ColTipoTelefone {
	
	String query;
	
    public TipoTelefone consultaCodTipoTelefone(TipoTelefone tipoTelefone) throws SQLException{
        
        query = "SELECT * FROM tipoTelefone WHERE nomeTipoFone = '" + tipoTelefone.getNomeTipo() + "';";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        tipoTelefone.setIdTipoTelefone(rs.getInt("idTipoFone"));
        
        return tipoTelefone; 
    }
    
    public TipoTelefone insereTipoTelefone(TipoTelefone tipoTelefone) throws SQLException {
        
        query = "INSERT INTO tipoTelefone (nomeTipoFone) VALUES ('" + tipoTelefone.getNomeTipo() + "')";

        Conexao.stm.executeUpdate(query);
        
        return tipoTelefone;

    }
    
    public TipoTelefone consultaTipoTelefone(TipoTelefone tipoTelefone) throws SQLException{
    	
    	ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM tipoTelefone WHERE idTipoTelefone = '" + tipoTelefone.getIdTipoTelefone() + "' LIMIT 1;");
        rs.last();
        tipoTelefone.setNomeTipo(rs.getString("nomeTipoFone"));
        
        return tipoTelefone;
    }
    
    public ResultSet resultSetTipoTelefone(int codTipoTelefone) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM tipoTelefone WHERE idTipoTelefone = '" + codTipoTelefone + "' LIMIT 1;");
        rs.last();
        return rs;

    }
}
