package unioeste.geral.endereco.col;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringEscapeUtils;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.exception.endereco.EnderecoException;


public class ColEndereco {
    
    String query;
    
    public Endereco consultaCodEndereco(Endereco end) throws SQLException {
     
        ColLogradouro cL = new ColLogradouro();
        ColCidade cC     = new ColCidade();
        ColBairro cB     = new ColBairro();
        ColPais cP       = new ColPais();

        Logradouro log = cL.consultaCodLogradouro(end.getLogradouro());
        Cidade cid     = cC.consultaCodCidade(end.getCidade());
        Bairro bar     = cB.consultaCodBairro(end.getBairro());
        Pais pai       = cP.consultaCodPais(end.getPais());
        
        query = "SELECT * FROM endereco WHERE "
                + "idCidade = '" + cid.getIdCidade() + "' AND "
                + "idBairro = '" + bar.getIdBairro() + "' AND "
                + "idLogradouro = '" + log.getIdLogradouro() + "' AND "
                + "idPais = '" + pai.getIdPais() + "' AND "
                + "cep  = '" + end.getCep() + "'";

        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        end.setIdEndereco(rs.getInt("idEndereco"));
        end.setCep(rs.getString("cep"));
        end.setCidade(cid);
        end.setBairro(bar);
        end.setLogradouro(log);
        end.setPais(pai);
        
        return end;
    }
    
        
    public Endereco insereEndereco(Endereco end) throws SQLException{
        
        try{
            end = consultaCodEndereco(end);
            return end;
            
        }
        catch(SQLException e){
            ColLogradouro cL = new ColLogradouro();
            ColCidade cC     = new ColCidade();
            ColBairro cB     = new ColBairro();
            ColPais cP       = new ColPais();
            
            try{
                end.setLogradouro(cL.consultaCodLogradouro(end.getLogradouro()));
                
            } catch (SQLException ex) {
                end.setLogradouro(cL.insereLogradouro(end.getLogradouro()));
                end.setLogradouro(cL.consultaCodLogradouro(end.getLogradouro()));
            }
            
            try {
                end.setCidade(cC.consultaCodCidade(end.getCidade()));
            } catch (SQLException ex) {
                end.setCidade(cC.insereCidade(end.getCidade()));
                end.setCidade(cC.consultaCodCidade(end.getCidade()));
            }
            
            try {
                end.setBairro(cB.consultaCodBairro(end.getBairro()));
            } catch (SQLException ex) {
                end.setBairro(cB.insereBairro(end.getBairro()));
                end.setBairro(cB.consultaCodBairro(end.getBairro()));
            }
            
            try {
                end.setPais(cP.consultaCodPais(end.getPais()));
            } catch (SQLException ex) {
                end.setPais(cP.inserePais(end.getPais()));
                end.setPais(cP.consultaCodPais(end.getPais()));
            }
            
            query = "INSERT INTO endereco (cep, idCidade, idBairro, idLogradouro, idPais) "
                + "VALUES" 
            	+ " ('" + end.getCep()  
                + "'," + end.getCidade().getIdCidade() 
            	+ " ," + end.getBairro().getIdBairro() 
                + " ," + end.getLogradouro().getIdLogradouro()  
            	+ " ," + end.getPais().getIdPais() + ")";

            Conexao.stm.executeUpdate(query);

            return end;
        }
    }

    public Endereco alterarEndereco(Endereco end) throws SQLException {
        
        ColLogradouro cL = new ColLogradouro();
        ColCidade cC     = new ColCidade();
        ColBairro cB     = new ColBairro();
        ColPais cP       = new ColPais();

        Logradouro log = cL.consultaCodLogradouro(end.getLogradouro());
        Cidade cid     = cC.consultaCodCidade(end.getCidade());
        Bairro bar     = cB.consultaCodBairro(end.getBairro());
        Pais pai       = cP.consultaCodPais(end.getPais());

        query = "UPDATE endereco SET "
                + "cep          = '" + end.getCep() + "', "
                + "idBairro     = " + bar.getIdBairro() + ", "
                + "idLogradouro = " + log.getIdLogradouro()+ ", "
                + "idCidade     = " + cid.getIdCidade() + ", "
                + "idPais       = " + pai.getIdPais() + " "
                + "WHERE idEndereco = " + end.getIdEndereco();

        Conexao.stm.executeUpdate(query);

        return end;
    }

    public Endereco excluirEndereco(Endereco end) throws SQLException {

        query = "DELETE FROM endereco WHERE idEndereco = " + end.getIdEndereco();
        Conexao.stm.executeUpdate(query);

        return end;
    }
    
    public ResultSet resultSetEndereco(int codEndereco) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM endereco WHERE idEndereco = '" + codEndereco + "' LIMIT 1;");
        rs.last();
        return rs;

    }
    
    public ResultSet resultSetEndereco(String cep) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM endereco WHERE cep = '" + cep + "' LIMIT 1;");
        rs.last();
        return rs;

    }
    
    
    public Endereco dePara(ResultSet rs) throws SQLException{
        ColLogradouro cL      = new ColLogradouro();
        ColCidade cC          = new ColCidade();
        ColBairro cB          = new ColBairro();
        ColPais cP            = new ColPais();
        ColUF cU              = new ColUF();
        
        //bugs do resultSet (ver como arrumar isso)
        //uma opcao � armazenar as parada do result set em algum arrayList temporario
        
        ResultSet rsCidade     = cC.resultSetCidade(rs.getInt("idCidade"));
        ResultSet rsBairro     = cB.resultSetBairro(rs.getInt("idBairro"));
        ResultSet rsLogradouro = cL.resultSetLogradouro(rs.getInt("idLogradouro"));
        ResultSet rsPais       = cP.resultSetPais(rs.getInt("idPais"));
        ResultSet rsUF         = cU.resultSetUF(rsCidade.getInt("idUF"));
        ResultSet rsTipoLog    = cU.resultSetUF(rsLogradouro.getInt("idTipoLogradouro"));
        
        int codEndereco         = rs.getInt("idEndereco");
        int codCidade           = rs.getInt("idCidade");
        int codBairro           = rs.getInt("idBairro");
        int codLogradouro       = rs.getInt("idLogradouro");
        int codPais             = rs.getInt("idPais");
        int codUF               = rsCidade.getInt("idUF");
        int codTLog             = rsTipoLog.getInt("idTipoLogradouro");
        
        
        String cep              = rs.getString("cep");
        String nomeCidade       = rsCidade.getString("nomeCidade");
        String nomeUF           = rsUF.getString("nomeUF");
        String siglaUF          = rsUF.getString("siglaUF");
        String nomePais         = rsPais.getString("nomePais");
        String siglaPais        = rsPais.getString("siglaPais");
        String nomeBairro       = rsBairro.getString("nomeBairro");
        String nomeLogradouro   = rsLogradouro.getString("nomeLogradouro");
        String nomeTipoLog      = rsTipoLog.getString("nomeTipo");
        String siglaTipoLog     = rsTipoLog.getString("siglaTipo");
        

        Endereco end = new Endereco(codEndereco, codBairro, codUF, codCidade, codPais, 
                codTLog, codLogradouro, cep, nomePais, siglaPais, nomeCidade, 
                nomeBairro, nomeUF, siglaUF, nomeLogradouro, nomeTipoLog, siglaTipoLog);
        
        return end;
    }
    
    
    public static ArrayList<String> splitEnderecoSite(String endereco) {
        endereco = endereco.trim();

        ArrayList<String> lista = new ArrayList<>();

        do {
            for (int i = 0; i < endereco.length(); i++) {
                if (i == (endereco.length() - 1)) {
                    lista.add(endereco.substring(0, i + 1).trim());
                    endereco = "";
                    break;
                } else if ((endereco.charAt(i) == '.') || (endereco.charAt(i) == ',') || (endereco.charAt(i) == '-')
                        || (endereco.charAt(i) == '	') || (endereco.charAt(i) == '/')
                        || (endereco.charAt(i) == ':') || (endereco.charAt(i) == '(') || (endereco.charAt(i) == ')')) {

                    if (i != 0) {
                        if (endereco.charAt(i) == '-' || endereco.charAt(i) == '.') {
                            if (!("0123456789".contains("" + endereco.charAt(i - 1)) && "0123456789".contains("" + endereco.charAt(i + 1)))) {
                                lista.add(endereco.substring(0, i).trim());
                                endereco = endereco.substring(i + 1, endereco.length());
                                break;
                            }
                        } else {
                            lista.add(endereco.substring(0, i).trim());
                            endereco = endereco.substring(i + 1, endereco.length());
                            break;
                        }
                    } else {
                        endereco = endereco.substring(i + 1, endereco.length());
                        break;
                    }
                }
            }
        } while (!endereco.isEmpty());

        for (int i = lista.size() - 1; i >= 0; i--) {
            String s = lista.get(i);

            if (s.isEmpty()) {
                lista.remove(i);
            }
            if (s.equals("CEP") || s.equals("Tel") || s.equals("Telefone")) {
                lista.remove(i);
            }
            if (s.length() == 9 && s.contains("-") && s.charAt(4) == '-' && (s.contains("1") || s.contains("2") || s.contains("3") || s.contains("4") || s.contains("5") || s.contains("6") || s.contains("7") || s.contains("8") || s.contains("9") || s.contains("0"))) {
                lista.remove(i);
            }
            if (s.contains(".") && (s.contains("1") || s.contains("2") || s.contains("3") || s.contains("4") || s.contains("5") || s.contains("6") || s.contains("7") || s.contains("8") || s.contains("9") || s.contains("0"))) {
                lista.remove(i);
            }

            try {
                Integer.parseInt(s);
                lista.remove(i);
            } catch (Exception e) {}
        }

        return lista;
    }
    
    
    public Endereco consultaEnderecoPorSite(String href) throws EnderecoException, IOException{
        
        String site = "";
        String linha;
        URL oURL;
        String conteudoDecodificado = "";

        try {
            oURL = new URL(href);
        } catch (MalformedURLException ex) {
            throw new EnderecoException("URL invalida");
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(oURL.openStream()));

        while ((linha = in.readLine()) != null) {
            site += linha;
        }

        in.close();

        Pattern pattern = Pattern.compile(".*?<address>(.*?)</address>.*?");

        Matcher matcher = pattern.matcher(site);
        if (matcher.matches()) {
            String conteudo = matcher.group(1).replaceAll("\\<.*?>", "");
            conteudoDecodificado = StringEscapeUtils.unescapeHtml4(conteudo);

        }

        ArrayList<String> aux;
	aux = splitEnderecoSite(conteudoDecodificado);
        String[] arrayEndereco = new String[10];
        
        for(int j = 0; j < 10;j++){
            arrayEndereco[j] = "";
        }
        
        for(int i = 0; i < aux.size(); i++){
            arrayEndereco[i] = aux.get(i);
        }
        
        String siglaTipoLogradouro = arrayEndereco[0];
        String logradouro = arrayEndereco[1];
        String bairro = arrayEndereco[2];
        String cidade = arrayEndereco[3];
        String siglaEstado = arrayEndereco[4];
        String pais = arrayEndereco[6];
        String cep = arrayEndereco[5];
        String siglaPais = arrayEndereco[7];
        
        Endereco end = new Endereco(0,0,0,0,0,0,0, cep, pais, siglaPais, cidade, bairro, "", siglaEstado, logradouro, "", siglaTipoLogradouro);

        return end;
    }

}
