package unioeste.geral.endereco.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.Bairro;

public class ColBairro {  
    
    String query;
    
    public Bairro consultaCodBairro(Bairro bairro) throws SQLException{
        
        query = "SELECT idBairro FROM enderecoBairro WHERE nomeBairro = '" + bairro.getNome() + "';";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        bairro.setIdBairro(rs.getInt("idBairro"));
        
        return bairro; 
    }
    
    public Bairro insereBairro(Bairro bairro) throws SQLException {
        
        query = "INSERT INTO enderecoBairro (nomeBairro) "
              + "VALUES ('" + bairro.getNome() + "')";

        Conexao.stm.executeUpdate(query);
        
        return bairro;

    }
    
    public ResultSet resultSetBairro(int codBairro) throws SQLException {       	
        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM enderecoBairro WHERE idBairro = '" + codBairro + "'");
        rs.last();
        
        return rs;

    }

}
