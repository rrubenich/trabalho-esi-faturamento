/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.endereco.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.TipoLogradouro;

/**
 *
 * @author rafakx
 */
public class ColTipoLogradouro {
    
    String query;
    
    public TipoLogradouro consultaCodTipoLogradouro(TipoLogradouro tLog) throws SQLException{

        query = "SELECT idTipoLogradouro FROM tipoLogradouro WHERE nomeTipo = '" + tLog.getNome() + "';";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        tLog.setIdTipoLogradouro(rs.getInt("idTipoLogradouro"));
        
        return tLog; 
    }
    
    public TipoLogradouro insereTipoLogradouro(TipoLogradouro tLog) throws SQLException {
        
        query = "INSERT INTO tipoLogradouro (nomeTipo, siglaTipo) "
              + "VALUES ('" + tLog.getNome() + "','" + tLog.getSigla() + "')";

        Conexao.stm.executeUpdate(query);
        
        return tLog;

    }
    
        
    public ResultSet resultSetTipoLogradouro(int codTipoLogradouro) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM enderecoTipoLogradouro WHERE idTipoLogradouro = '" + codTipoLogradouro + "' LIMIT 1;");
        rs.last();
        return rs;

    }
}
