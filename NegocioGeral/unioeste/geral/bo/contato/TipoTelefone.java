package unioeste.geral.bo.contato;

public class TipoTelefone {
    private String nomeTipo;
    private int idTipoTelefone;

    public TipoTelefone(){
    	this.nomeTipo = "";
    	this.idTipoTelefone = 0;
    }
    
    public TipoTelefone(int idTipoTelefone, String nomeTipo){
    	this.idTipoTelefone = idTipoTelefone;
    	this.nomeTipo = nomeTipo;
    }
    
    public int getIdTipoTelefone() {
		return idTipoTelefone;
	}

	public void setIdTipoTelefone(int idTipoTelefone) {
		this.idTipoTelefone = idTipoTelefone;
	}

	public String getNomeTipo() {
        return nomeTipo;
    }

    public void setNomeTipo(String nomeTipo) {
        this.nomeTipo = nomeTipo;
    }
}
