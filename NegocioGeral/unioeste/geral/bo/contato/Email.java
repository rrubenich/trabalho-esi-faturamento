package unioeste.geral.bo.contato;

import java.io.Serializable;

import unioeste.geral.exception.endereco.EnderecoException;


public class Email implements Serializable{
    private String email;

    public Email(){
    	this.email = "";
    }
    
    public Email(String email){
    	this.email = email;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Email validaEmail(Email email) throws EnderecoException{
    	if(email.getEmail().length() == 0){
            throw new EnderecoException("Nome da cidade invalida");
        }
		return email;
    }
}
