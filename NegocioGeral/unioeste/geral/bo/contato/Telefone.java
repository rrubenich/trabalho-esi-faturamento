package unioeste.geral.bo.contato;

public class Telefone {
    private int DDD;
    private String numero;
    private TipoTelefone tipoTelefone;

    public Telefone(){
    	this.tipoTelefone = new TipoTelefone();
    	this.DDD = 0;
    	this.numero = "";
    }
    
    public Telefone(int ddd, String numero, TipoTelefone tipoTelefone){
    	this.DDD = ddd;
    	this.numero = numero;
    	this.tipoTelefone = tipoTelefone;
    }
    
    public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	public int getDDD() {
        return DDD;
    }

    public void setDDD(int DDD) {
        this.DDD = DDD;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
