/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.pessoa;

import java.util.ArrayList;

import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.documento.CNPJ;

public class PessoaJuridica extends Pessoa{
	private CNPJ numeroCNPJ;
	
    public PessoaJuridica(String nome, String segundoNome,
			EnderecoEspecifico enderecoEspecifico, ArrayList<Email> emails,
			ArrayList<Telefone> telefones, CNPJ numeroCNPJ) {
		super(nome, segundoNome, enderecoEspecifico, emails, telefones);
		this.numeroCNPJ = numeroCNPJ;
	}


    public CNPJ getNumeroCNPJ() {
        return numeroCNPJ;
    }

    public void setNumeroCNPJ(CNPJ numeroCNPJ) {
        this.numeroCNPJ = numeroCNPJ;
    }
}
