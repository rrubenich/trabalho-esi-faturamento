package unioeste.geral.bo.pessoa;

import java.io.Serializable;

import unioeste.geral.exception.pessoa.PessoaException;


public class Sexo implements Serializable{
    private String sigla;
    private String nome;

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Sexo validaSexo(Sexo sexo) throws PessoaException{
    	if(sexo.getSigla() == "F" || sexo.getNome() == "M"){
    		return sexo;
    	}else{
    		throw new PessoaException("Sexo inválido");
    	}
    }
}
