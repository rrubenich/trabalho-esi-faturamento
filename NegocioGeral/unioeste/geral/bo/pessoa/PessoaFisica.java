package unioeste.geral.bo.pessoa;

import java.util.ArrayList;

import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.documento.*;

public class PessoaFisica extends Pessoa{
    private CPF numeroCPF;
    private Sexo sexo;
    private DocID numeroDocNacional;

    public CPF getNumeroCPF() {
        return numeroCPF;
    }

    public void setNumeroCPF(CPF numeroCPF) {
        this.numeroCPF = numeroCPF;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public DocID getNumeroDocNacional() {
        return numeroDocNacional;
    }

    public void setNumeroDocNacional(DocID numeroDocNacional) {
        this.numeroDocNacional = numeroDocNacional;
    }

	public PessoaFisica(String nome, String segundoNome,
			EnderecoEspecifico enderecoEspecifico, ArrayList<Email> emails,
			ArrayList<Telefone> telefones, CPF numeroCPF, Sexo sexo,
			DocID numeroDocNacional) {
		super(nome, segundoNome, enderecoEspecifico, emails, telefones);
		this.numeroCPF = numeroCPF;
		this.sexo = sexo;
		this.numeroDocNacional = numeroDocNacional;
	}

    
}
