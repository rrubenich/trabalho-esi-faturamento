/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.pessoa;

import unioeste.geral.bo.endereco.Endereco;

/**
 *
 * @author rafakx
 */
public class EnderecoEspecifico {
    int numero;
    String complemento;
    Endereco endereco;

    public EnderecoEspecifico(){
    	this.complemento = "";
    	this.numero = 0;
    	setEndereco(new Endereco());
    }
    
    public EnderecoEspecifico(int numero, String complemento, Endereco endereco){
    	this.numero = numero;
    	this.complemento = complemento;
    	this.endereco = endereco;
    }
    
    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
}
