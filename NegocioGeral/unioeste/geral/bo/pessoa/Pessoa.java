package unioeste.geral.bo.pessoa;

import java.util.ArrayList;

import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.exception.pessoa.PessoaException;

public class Pessoa {
    private String nome;
    private String segundoNome;
    private EnderecoEspecifico enderecoEspecifico;
    private ArrayList<Email> emails;
    private ArrayList<Telefone> telefones;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSegundoNome() {
        return segundoNome;
    }

    public void setSegundoNome(String segundoNome) {
        this.segundoNome = segundoNome;
    }

    public EnderecoEspecifico getEnderecoEspecifico() {
        return enderecoEspecifico;
    }

    public void setEnderecoEspecifico(EnderecoEspecifico endereco) {
        this.enderecoEspecifico = endereco;
    }

    public ArrayList<Email> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<Email> emails) {
        this.emails = emails;
    }

    public ArrayList<Telefone> getTelefones() {
        return telefones;
    }
    public void setTelefones(ArrayList<Telefone> telefones){
    	this.telefones = telefones;
    }
    
    public Pessoa validaPessoa(Pessoa pessoa) throws PessoaException{
    	if(pessoa.getNome().length() < 5){
    		throw new PessoaException("Nome inválido");
    	}
    	if(pessoa.getNome().length() < 5){
    		throw new PessoaException("Segundo nome inválido");
    	}
    	
    	return pessoa;
    	
    }

	public Pessoa(String nome, String segundoNome,
			EnderecoEspecifico enderecoEspecifico, ArrayList<Email> emails,
			ArrayList<Telefone> telefones) {
		super();
		this.nome = nome;
		this.segundoNome = segundoNome;
		this.enderecoEspecifico = enderecoEspecifico;
		this.emails = emails;
		this.telefones = telefones;
	}
}
