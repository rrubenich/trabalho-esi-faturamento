/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.documento;

/**
 *
 * @author rafakx
 */
public class DocFiscal {
    private String numeroDocFiscal;

    public String getNumeroDocFiscal() {
        return numeroDocFiscal;
    }

    public void setNumeroDocFiscal(String numeroDocFiscal) {
        this.numeroDocFiscal = numeroDocFiscal;
    }

	public DocFiscal(String numeroDocFiscal) {
		super();
		this.numeroDocFiscal = numeroDocFiscal;
	}
}
