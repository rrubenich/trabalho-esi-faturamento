package unioeste.geral.bo.documento;

public class DocID{
    private DocIDBrasil docIDBrasil;

	public DocIDBrasil getDocIDBrasil() {
		return docIDBrasil;
	}

	public void setDocIDBrasil(DocIDBrasil docIDBrasil) {
		this.docIDBrasil = docIDBrasil;
	}

	public DocID(DocIDBrasil docIDBrasil) {
		super();
		this.docIDBrasil = docIDBrasil;
	}
	
}
