package unioeste.geral.bo.documento;

public class DocIDBrasil{
    private String numeroRG;
    private OrgaoExp orgaoExp;

    public DocIDBrasil(String numeroRG, OrgaoExp orgaoExp) {
		super();
		this.numeroRG = numeroRG;
		this.orgaoExp = orgaoExp;
	}

	public String getNumeroRG() {
        return numeroRG;
    }

    public void setNumeroRG(String numeroRG) {
        this.numeroRG = numeroRG;
    }

    public OrgaoExp getOrgaoExp() {
        return orgaoExp;
    }

    public void setOrgaoExp(OrgaoExp orgaoExp) {
        this.orgaoExp = orgaoExp;
    }
    
    
}
