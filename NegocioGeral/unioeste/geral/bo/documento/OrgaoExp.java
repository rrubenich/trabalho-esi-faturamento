package unioeste.geral.bo.documento;

import unioeste.geral.bo.endereco.UF;

public class OrgaoExp {
	private int idOrgaoExp;
    private String nomeOrgao;
    private UF uf;

    
    
    public OrgaoExp(int idOrgaoExp, String nomeOrgao, UF uf) {
		super();
		this.idOrgaoExp = idOrgaoExp;
		this.nomeOrgao = nomeOrgao;
		this.uf = uf;
	}
    
    public OrgaoExp(){
    	
    }

	public int getIdOrgaoExp() {
		return idOrgaoExp;
	}

	public void setIdOrgaoExp(int idOrgaoExp) {
		this.idOrgaoExp = idOrgaoExp;
	}

	public String getNomeOrgao() {
        return nomeOrgao;
    }

    public void setNomeOrgao(String nomeOrgao) {
        this.nomeOrgao = nomeOrgao;
    }

    public UF getUf() {
        return uf;
    }

    public void setUf(UF uf) {
        this.uf = uf;
    }
}
