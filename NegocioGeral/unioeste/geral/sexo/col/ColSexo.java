package unioeste.geral.sexo.col;


import java.util.ArrayList;

import unioeste.geral.bo.pessoa.Sexo;
import unioeste.geral.exception.pessoa.PessoaException;


public class ColSexo {
    public Sexo consultaSexo(Sexo sexo) throws PessoaException{
    	
    	ArrayList<Sexo> sexos = new ArrayList<Sexo>();
    	sexos = obterListaSexo();
    	
    	for(int i = 0; i<sexos.size();i++){
        	if(sexos.get(i).getSigla() == sexo.getSigla()){
        		return sexos.get(i);
        	}
        }
    	
    	throw new PessoaException("Sexo indefinido");

    }
    

    public ArrayList<Sexo> obterListaSexo(){
        
    	ArrayList<Sexo> sexos = new ArrayList<Sexo>();
    	
    	Sexo m = new Sexo();
    	m.setSigla("M");
    	m.setNome("Masculino");
    	sexos.add(m);
    	
    	Sexo f = new Sexo();
    	f.setSigla("F");
    	f.setNome("Feminino");
    	sexos.add(f);
    	
    	return sexos;
    	
    }
}
