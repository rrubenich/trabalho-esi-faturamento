/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.documento.col;

import java.sql.ResultSet;
import java.sql.SQLException;

import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.documento.OrgaoExp;
import unioeste.geral.endereco.col.ColUF;

public class ColOrgaoExp {
	String query;
    
    public OrgaoExp consultaCodOrgaoExp(OrgaoExp orgaoExp) throws SQLException{
        
        query = "SELECT idOrgao FROM orgaoExp WHERE nomeOrgao = '" + orgaoExp.getNomeOrgao() + "';";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        orgaoExp.setIdOrgaoExp(rs.getInt("idOrgaoExp"));
        
        return orgaoExp; 
    }
    
    public OrgaoExp insereOrgaoExp(OrgaoExp orgaoExp) throws SQLException {
        
    	ColUF cF = new ColUF();

    	try{
    		orgaoExp.setUf(cF.consultaCodUF(orgaoExp.getUf()));
    		
    	}catch(SQLException ex){
    		cF.insereUF(orgaoExp.getUf());
    		orgaoExp.setUf(cF.consultaCodUF(orgaoExp.getUf()));
    	}
    	
        query = "INSERT INTO orgaoExp (nomeOrgao, idUF) VALUES ('" + orgaoExp.getNomeOrgao() + "', " + orgaoExp.getUf().getIdUF() + ")";

        Conexao.stm.executeUpdate(query);
        
        return orgaoExp;

    }
    
    public ResultSet resultSetOrgaoExp(int codOrgaoExp) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM orgaoExp WHERE idOrgao = '" + codOrgaoExp + "' LIMIT 1;");
        rs.last();
        return rs;

    }
    
    
    public OrgaoExp dePara(ResultSet rs) throws SQLException{
    	OrgaoExp org = new OrgaoExp();

    	org.setIdOrgaoExp(rs.getInt("idOrgao"));
    	org.setNomeOrgao(rs.getString("nomeOrgao"));
    	
		return org;
    }
}
