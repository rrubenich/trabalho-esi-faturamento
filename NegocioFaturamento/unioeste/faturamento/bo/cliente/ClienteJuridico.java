
package unioeste.faturamento.bo.cliente;

import java.util.ArrayList;

import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.documento.CNPJ;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.PessoaJuridica;

public class ClienteJuridico extends PessoaJuridica{

	public ClienteJuridico(String nome, String segundoNome,
			EnderecoEspecifico enderecoEspecifico, ArrayList<Email> emails,
			ArrayList<Telefone> telefones, CNPJ numeroCNPJ) {
		super(nome, segundoNome, enderecoEspecifico, emails, telefones, numeroCNPJ);
		// TODO Auto-generated constructor stub
	}

    
}
