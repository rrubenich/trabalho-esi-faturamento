package unioeste.faturamento.bo.cliente;

import java.util.ArrayList;

import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.documento.CPF;
import unioeste.geral.bo.documento.DocID;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.PessoaFisica;
import unioeste.geral.bo.pessoa.Sexo;

public class ClienteFisico extends PessoaFisica{

	public ClienteFisico(String nome, String segundoNome,
			EnderecoEspecifico enderecoEspecifico, ArrayList<Email> emails,
			ArrayList<Telefone> telefones, CPF numeroCPF, Sexo sexo,
			DocID numeroDocNacional) {
		super(nome, segundoNome, enderecoEspecifico, emails, telefones, numeroCPF, sexo, numeroDocNacional);
	}
	
}
