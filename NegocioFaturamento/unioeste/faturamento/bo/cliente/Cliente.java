package unioeste.faturamento.bo.cliente;

import java.io.Serializable;

import unioeste.faturamento.exception.cliente.ClienteException;

public class Cliente implements Serializable{
	int idCliente;
	private ClienteFisico   dadosClienteFisico;
	private ClienteJuridico dadosClienteJuridico;
	
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
    public ClienteFisico getDadosClienteFisico() {
		return dadosClienteFisico;
	}

	public void setDadosClienteFisico(ClienteFisico dadosClienteFisico) {
		this.dadosClienteFisico = dadosClienteFisico;
	}

	public ClienteJuridico getDadosClienteJuridico() {
		return dadosClienteJuridico;
	}

	public void setDadosClienteJuridico(ClienteJuridico dadosClienteJuridico) {
		this.dadosClienteJuridico = dadosClienteJuridico;
	}
	
	public Cliente validaCliente(Cliente cli) throws ClienteException{

    	if(cli.getDadosClienteFisico() != null){
    		if (cli.getDadosClienteFisico().getNome().length() < 5) {
				throw new ClienteException("Nome inválido");
			}
    		else if(cli.getDadosClienteFisico().getNumeroCPF().getNumeroDocFiscal().length() < 11){
    			throw new ClienteException("CPF inválido");
    		}
    		else if(cli.getDadosClienteFisico().getNumeroDocNacional().getDocIDBrasil().getNumeroRG().length() < 5){
    			throw new ClienteException("RG inválido");
    		}
    	}else{
    		if (cli.getDadosClienteJuridico().getNome().length() < 5) {
				throw new ClienteException("Nome inválido");
			}
    		else if(cli.getDadosClienteJuridico().getNumeroCNPJ().getNumeroDocFiscal().length() > 14){
    			throw new ClienteException("CNPJ inválido");
    		}
    	}
		
		return cli;
		
	}

}

