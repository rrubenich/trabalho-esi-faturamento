
package unioeste.faturamento.bo.fabricante;

import java.util.ArrayList;

import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.documento.CNPJ;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.PessoaJuridica;

public class Fabricante extends PessoaJuridica{
	int id;    
	
	public Fabricante(String nome, String segundoNome,
		EnderecoEspecifico enderecoEspecifico, ArrayList<Email> emails,
		ArrayList<Telefone> telefones, CNPJ numeroCNPJ) {
		
		super(nome, segundoNome, enderecoEspecifico, emails, telefones, numeroCNPJ);
	}
    
	public Fabricante() {    	
		super(null,null,null,null,null,null);
		setEnderecoEspecifico(new EnderecoEspecifico());
		setEmails(new ArrayList<Email>());
		setTelefones(new ArrayList<Telefone>());
		setNumeroCNPJ(new CNPJ(null));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
    
}
