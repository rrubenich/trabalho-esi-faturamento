package unioeste.view.teste;

import java.util.ArrayList;

import unioeste.apoio.mysql.Conexao;
import unioeste.faturamento.bo.fabricante.Fabricante;
import unioeste.faturamento.manager.UCFabricanteFaturamentoServicos;
import unioeste.geral.bo.contato.Email;
import unioeste.geral.bo.contato.Telefone;
import unioeste.geral.bo.contato.TipoTelefone;
import unioeste.geral.bo.documento.CNPJ;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.bo.endereco.UF;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;

public class Teste{
	
	public static void main(String[] x){
		
		Conexao.conectar();
		
		Pais pais = new Pais(0, "Brasil", "BRA");
		Bairro bairro = new Bairro(0,"Centro");
		UF uf = new UF(0, "Paraná", "PR");
		Cidade cidade = new Cidade(0, "Foz do Iguaçu", uf);
		TipoLogradouro tipoLogradouro = new TipoLogradouro(0, "Avenida", "Av.");
		Logradouro logradouro = new Logradouro(0, "Brasil", tipoLogradouro);
		Endereco end = new Endereco(0, "85875-000", pais, bairro, cidade, logradouro);
		EnderecoEspecifico endEsp = new EnderecoEspecifico(564,"Edificio na esquina",end);
		
		ArrayList<Email> listaEmails = new ArrayList<Email>();
		Email email1 = new Email("faq@cocacola.com.br");
		listaEmails.add(email1);
		Email email2 = new Email("suporte@cocacola.com.br");
		listaEmails.add(email2);
		Email email3 = new Email("sugestoes@cocacola.com.br");
		listaEmails.add(email3);
		
		ArrayList<Telefone> listaTelefones = new ArrayList<Telefone>();
		TipoTelefone tipoTelefone = new TipoTelefone(0,"Comercial");
		Telefone telefone1 = new Telefone(45,"3543-3431",tipoTelefone);
		listaTelefones.add(telefone1);
		Telefone telefone2 = new Telefone(45,"3452-2353",tipoTelefone);
		listaTelefones.add(telefone2);
		
		CNPJ cnpj = new CNPJ("78.425.986/0036-15");
		
		Fabricante fab = new Fabricante("Coca-Cola Industrias Ltda","Coca-Cola",endEsp,listaEmails,listaTelefones,cnpj);
		
		UCFabricanteFaturamentoServicos ucFabricante = new UCFabricanteFaturamentoServicos();
		try {
			ucFabricante.cadastrarFabricante(fab);
			
			fab = ucFabricante.obterListaFabricante().get(ucFabricante.obterListaFabricante().size()-1);
			fab.setSegundoNome("Cocal-Cola Ind. Ltda.");
			ucFabricante.alterarFabricante(fab);
			
			//ucFabricante.excluirFabricante(fab);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Fim execução!");
		
		Conexao.fechar();
	}
	
}