package unioeste.faturamento.exception.cliente;

public class ClienteException extends Exception {
    private String mensagem;
    
    public ClienteException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
}
