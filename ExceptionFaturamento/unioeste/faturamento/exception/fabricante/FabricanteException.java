package unioeste.faturamento.exception.fabricante;

public class FabricanteException extends Exception{
    private String mensagem;
    
    public FabricanteException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
}
